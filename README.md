# canal-rabbitmq-redis

#### 介绍
实现缓存和数据库数据一致性方案实战

#### 软件架构
![输入图片说明](https://foruda.gitee.com/images/1678857282572098372/92a697c9_1006347.png "1整体架构.png")


#### 安装教程

1.  [mysql 安装](https://mp.weixin.qq.com/s?__biz=MzI5MDg2NjEzNA==&amp;mid=2247484053&amp;idx=1&amp;sn=6eec591ceb3980c9b7e93ca2acf738f9&amp;chksm=ec18142cdb6f9d3a340b74c7f1ae392f030ea0b5fad60ab7e822f2eb444a086867f340ec6ef1&token=933180097&lang=zh_CN#rd)
2.  [canal安装](https://mp.weixin.qq.com/s/-KO6fe6bMl0lMFDbdM6pSw)
3.  [RabbitMQ安装](https://mp.weixin.qq.com/s?__biz=MzI5MDg2NjEzNA==&amp;mid=2247488054&amp;idx=2&amp;sn=c0d7684042faf7d5f6add615f83b8082&amp;chksm=ec18048fdb6f8d99fefd95b65fbde9220817b236d5013e87008cb5c1b339453b51eee59b2058&token=933180097&lang=zh_CN#rd)
4. [Redis安装](https://mp.weixin.qq.com/s?__biz=MzI5MDg2NjEzNA==&amp;mid=2247484058&amp;idx=1&amp;sn=a18a1bbd308a63dbf51ddbfaf0128e04&amp;chksm=ec181423db6f9d3587c45b57590fe2b80a078280f58a64e71d977cf11a6572eeeaabf2c0604c&token=933180097&lang=zh_CN#rd)

#### 使用说明

1.  canal 修改 `conf/canal.properties` 配置
```yml
# 指定模式
canal.serverMode = rabbitMQ
# 指定实例,多个实例使用逗号分隔: canal.destinations = example1,example2
canal.destinations = example 

# rabbitmq 服务端 ip
rabbitmq.host = 127.0.0.1
# rabbitmq 虚拟主机 
rabbitmq.virtual.host = / 
# rabbitmq 交换机  
rabbitmq.exchange = xxx
# rabbitmq 用户名
rabbitmq.username = xxx
# rabbitmq 密码
rabbitmq.password = xxx
rabbitmq.deliveryMode =
```

2. 修改实例配置文件 `conf/example/instance.properties`
```yml
#配置 slaveId,自定义,不等于 mysql 的 server Id 即可
canal.instance.mysql.slaveId=10 

# 数据库地址:配置自己的ip和端口
canal.instance.master.address=ip:port 
 
# 数据库用户名和密码 
canal.instance.dbUsername=xxx 
canal.instance.dbPassword=xxx
	
# 指定库和表
canal.instance.filter.regex=.*\\..*    // 这里的 .* 表示 canal.instance.master.address 下面的所有数据库
		
# mq config
# rabbitmq 的 routing key
canal.mq.topic=xxx
```

3. 记得修改项目中 application-loc.yml 中的配置

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

#### 联系作者
欢迎关注公众号：

![输入图片说明](src/test/java/com/itcast/cheetah/crr/%E5%85%AC%E4%BC%97%E5%8F%B7%E4%BA%8C%E7%BB%B4%E7%A0%81.jpg)

联系作者：

![输入图片说明](src/test/java/com/itcast/cheetah/crr/%E4%BA%8C%E7%BB%B4%E7%A0%81.jpg)

如果大家想进行技术交流，可以备注：技术群，进入阿Q专属读者群

如果大家想赚点外块，可以备注：任务群，进入有偿任务返现群，获取8-15元的小额任务返现。



