package com.itcast.cheetah.crr.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itcast.cheetah.crr.product.entity.ProductInfo;

/**
 * @ClassName: ProductInfoService
 * @Description: 商品service
 * @Date: 2023/3/10
 * @Author: cheetah
 * @Version: 1.0
 */
public interface IProductInfoService extends IService<ProductInfo> {

    ProductInfo findProductInfo(Long productInfoId);
}
