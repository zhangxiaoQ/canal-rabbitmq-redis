package com.itcast.cheetah.crr.product.model;

import com.itcast.cheetah.crr.product.entity.ProductInfo;
import lombok.Data;

import java.util.List;

@Data
public class ProductInfoDetail {

    private List<ProductInfo> data;

    private String database;

    private String table;
}
