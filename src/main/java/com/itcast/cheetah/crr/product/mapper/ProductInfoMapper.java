package com.itcast.cheetah.crr.product.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itcast.cheetah.crr.product.entity.ProductInfo;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ProductInfoMapper extends BaseMapper<ProductInfo> {
}
