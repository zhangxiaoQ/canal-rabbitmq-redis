package com.itcast.cheetah.crr.product.controller;

import com.itcast.cheetah.crr.common.AjaxResult;
import com.itcast.cheetah.crr.product.entity.ProductInfo;
import com.itcast.cheetah.crr.product.service.IProductInfoService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

/**
 * @ClassName: ProductInfoController
 * @Description: 商品类
 * @Date: 2023/3/10
 * @Author: cheetah
 * @Version: 1.0
 */
@RequiredArgsConstructor
@RestController
@RequestMapping("/productInfo")
public class ProductInfoController {

    @Autowired
    private IProductInfoService productInfoService;


    /**
     * 查询商品消息
     * @param productInfoId
     * @return
     */
    @GetMapping("/findProductInfo")
    public AjaxResult get(@RequestParam Long productInfoId){
        ProductInfo productInfo = productInfoService.findProductInfo(productInfoId);
        if(productInfo!=null){
            return AjaxResult.success(productInfo);
        }
        return AjaxResult.error("你查找的商品信息不存在！");
    }

    /**
     * 更新商品信息
     * @param productInfo
     * @return
     */
    @PostMapping("/update")
    public AjaxResult update(@RequestBody ProductInfo productInfo){
        productInfoService.updateById(productInfo);
        return AjaxResult.success();
    }
}
